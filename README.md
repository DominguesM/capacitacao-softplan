# Capacitação em Inteligência Artificial **Softplan**

<img align="right" height="180" src="https://cdn3.iconfinder.com/data/icons/fantasy-and-role-play-game-adventure-quest/512/Dragon_Egg-512.png">


ATENÇÃO:

O todos os níveis são compostos por dois (2) dias contendo 4 horas de conteúdo cada. Antes de se inscrever verifique abaixo os dias e horários:

```

    Nível 1:


    Turma 6: 15 e 16/07, das 14:00 às 17:00.
    Turma 7: 26 e 27/08, das 14:00 às 17:00.
    Turma 8: 09 e 10/09, das 14:00 às 17:00.


    Nível 2:


    Turma 1: 05 e 06/08, das 14:00 às 17:00.
    Turma 2: 19 e 20/08, das 14:00 às 17:00.
    Turma 3: 16 e 17/09, das 14:00 às 17:00.
```

## Nível 1

Primeiro nível de uma série de cinco capacitações oferecidos pelo Centro de Competência em Inteligência Artificial.
No primeiro dia vamos revisar todos os conceitos e principais técnicas da área de aprendizagem de maquina,
já no segundo vamos ver uma introdução ao pré-processamento de dados e utilização de API's para o 
treinamento de um classificador de texto.

**Requisitos**:

**Primeiro Dia**:

* Nenhum

**Segundo Dia:**

* Notebook
* Conhecimento básico em linguagem de programação (Python)


## Nível 2

O segundo nível é composto pela teoria das principais técnicas (algorítimos) utilizados para a classificação de texto, extração de entidades nomeadas e clusterização de texto e também a parte de desenvolvimento e utilização dessas técnicas.

**Requisitos**:

**Primeiro Dia**:

* Nível 1 ou conhecimento básico em inteligencia artificial.

**Segundo Dia:**

* Notebook
* Conhecimento básico em linguagem de programação (Python)